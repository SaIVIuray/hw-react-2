import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import './Header.scss'

export default function Header({ cartItemTotal, favoriteItemTotal}) {

    return (
        <header className="header">
              <div className="container header-wrapper">
                  <h1 className="title"> <img  className='title-img'src="/public/images/icone-vecteur-logo-soins-pour-animaux-compagnie-illustration-fond-noir_683738-5069.jpg" alt="" /> Jack&Pets</h1>
                  <div className="icons-wrapper">


                  <div className="star-wrapper">
                          <FontAwesomeIcon
                              icon={faStar}
                              size="lg"
                             
                          />{" "}
                          <span>{favoriteItemTotal}</span>
                      </div>

                      <div
                          className="cart-shopping-wrapper" 
                      >
                          <FontAwesomeIcon
                              icon={faCartShopping}
                              size="lg"
                              
                          />{" "}
                          <span>{cartItemTotal}</span>
                      </div>

                     
                  </div>
              </div>
          </header>




    )
}
