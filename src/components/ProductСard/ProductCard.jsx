import Button from "../Button/Button";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { faStar } from "@fortawesome/free-solid-svg-icons";
import './ProductCard.scss'




export default function ProductCard(props) {

    const { name, price, image } = props.product;
    const { inCart, inFavorites } = props;


    const handleClickAddToCart = () => {
        props.showCartModal(props.product);
        props.addToCart(props.product);
    };

    const handleClickAddToFavorites = () => {
        props.addToFavorites(props.product);
    };




    return (
        <div className="product-card">
            <div className="image-wrapper">
                <img src={image} alt={name} />
                {!inFavorites && (
                    <Button
                        classNames="add-favorite"
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="lg"
                            style={{ color: "grey" }}
                        />
                    </Button>
                )}
                {inFavorites && (
                    <Button
                        classNames="add-favorite"
                        style={{
                            backgroundColor: "#fff",
                            borderRadius: "60px",
                        }}
                        onClick={handleClickAddToFavorites}
                    >
                        <FontAwesomeIcon
                            icon={faStar}
                            size="lg"
                            style={{ color: "yellow" }}
                        />
                    </Button>
                )}


            </div>
            <h3 className="product-name">{name}</h3>
            <p className="product-price">{price} $</p>
                
             
                
           
            {!inCart && (
                <Button classNames="add-to-cart" onClick={ handleClickAddToCart}>
                    Add to cart
                </Button>
            )}
            {inCart && (
                <Button
                    classNames="remove-to-cart"
                >
                    Item in cart
                </Button>
            )} 
            </div>
    );
}


ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
};


