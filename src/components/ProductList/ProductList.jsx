import './ProductList.scss'
import PropTypes from "prop-types";
import ProductCard from "../ProductСard/ProductCard";

export default function ProductList(props) {
  const {
      products,
      cartItems,
      favoriteItems,
      addToCart,
      addToFavorites,
      showCartModal
  } = props;
  return (
      <div className="container">
          <div className="product-list">
        
              {products.map((product) => (
                  <ProductCard
                      key={product.id}
                      product={product}
                      inCart={cartItems.some(
                          (item) => item.id === product.id
                      )}
                      inFavorites={favoriteItems.some(
                          (item) => item.id === product.id
                      )}
                      addToCart={addToCart}
                      addToFavorites={addToFavorites}
                      showCartModal={showCartModal}
                      
                  />
              ))}
          </div>
      </div>
  );
}

ProductList.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
};



